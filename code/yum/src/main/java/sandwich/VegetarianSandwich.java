package sandwich;

public abstract class VegetarianSandwich implements ISandwich {
    public String filling;

    /**
     * Creates a constructor for VegetarianSandwich
     * @param filling The filling of the vegetarian sandwich
     */
    public VegetarianSandwich (String filling){
        this.filling = filling;
    }

    public String getFilling(){
        return this.filling;
    }

    public abstract String getProtein();
    
    public void addFilling(String filling){
        String[] blackListedFilling = {"chicken", "beef", "fish", "meat", "pork"};
        for (String blFilling : blackListedFilling) {
            if (blFilling.equals(filling)) {
                throw new IllegalArgumentException("Cannot put non-vegetarian filling " + filling + " in a vegetarian sandwich.");
            }
        }

        if (this.filling != ""){
            this.filling += ", ";
        }
        this.filling += filling;
    }

    public boolean isVegetarian(){
        return true;
    }

    public boolean isVegan(){
        return !(this.filling.contains("cheese") || this.filling.contains("egg"));
    }

}

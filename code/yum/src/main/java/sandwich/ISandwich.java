package sandwich;

public interface ISandwich {
    /**
     * Gets the filling of the sandwich
     * @return List of fillings, comma-separated.
     */
    String getFilling();

    /**
     * Adds a filling to the sandwich
     * @param filling The filling.
     */
    void addFilling(String filling);

    /**
     * Says if the sandwich is vegetarian or not.
     * @return Is vegetarian.
     */
    boolean isVegetarian();
}
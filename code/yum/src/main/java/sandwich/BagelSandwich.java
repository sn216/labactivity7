package sandwich;

public class BagelSandwich implements ISandwich {
    private String filling;

    /**
     * Creates a new BagelSandwich
     * @param filling The filling of the BagelSandwich.
     */
    public BagelSandwich(String filling) {
        this.filling = filling;
    }

    public String getFilling() {
        return this.filling;
    }

    public void addFilling(String filling) {
        if (this.filling != "") {
            this.filling += ", ";
        }
        this.filling += filling;
    }

    public boolean isVegetarian() {
        throw new UnsupportedOperationException();
    }

}

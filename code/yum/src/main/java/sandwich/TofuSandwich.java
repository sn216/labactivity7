package sandwich;

public class TofuSandwich extends VegetarianSandwich {
    public TofuSandwich() {
        super("tofu");
    }

    @Override
    public String getProtein() {
        return "tofu";
    }
}

package sandwich;

public class CeasarSandwich extends VegetarianSandwich{
   
    public CeasarSandwich(){
        super("Ceasar Dressing");
    }

    @Override
    public String getProtein(){
        return "Anchovies";
    }

    @Override
    public boolean isVegetarian(){
        return false;
    }
}

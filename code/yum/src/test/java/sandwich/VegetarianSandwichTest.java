package sandwich;

import static org.junit.Assert.*;
import org.junit.Test;

public class VegetarianSandwichTest {
    @Test
    public void TofuSandwichConstructor() {
        new TofuSandwich();
    }

    @Test
    public void TofuSandwich_getFilling() {
        TofuSandwich s = new TofuSandwich();
        assertEquals("Wrong filling returned", "tofu", s.getFilling());
    }
    
    @Test
    public void TofuSandwich_getProtein() {
        TofuSandwich s = new TofuSandwich();
        assertEquals("Wrong protein returned", "tofu", s.getFilling());
    }

    @Test
    public void TofuSandwich_addFilling_then_getFilling() {
        TofuSandwich s = new TofuSandwich();
        s.addFilling("lettuce");
        assertEquals("Wrong filling returned", "tofu, lettuce", s.getFilling());
    }

    @Test
    public void TofuSandwich_isVegetarian() {
        TofuSandwich s = new TofuSandwich();
        assertTrue(s.isVegetarian());
    }

    @Test
    public void CeasarSandwich_getFilling() {
        CeasarSandwich s = new CeasarSandwich();
        assertEquals("Wrong filling returned", "Ceasar Dressing", s.getFilling());
    }

    @Test
    public void CeasarSandwich_getProtein() {
        CeasarSandwich s = new CeasarSandwich();
        assertEquals("Wrong protein returned", "Anchovies", s.getProtein());
    }

    @Test
    public void CeasarSandwich_isVegetarian() {
        CeasarSandwich s = new CeasarSandwich();
        assertFalse(s.isVegetarian());
    }
}

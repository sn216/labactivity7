package sandwich;

import static org.junit.Assert.*;
import org.junit.Test;

public class BagelSandwichTest {
    
    @Test
    public void bagelSandwichConstructor(){
        BagelSandwich s = new BagelSandwich ("lettuce, bacon");
    }

    @Test
    public void bagelSandwichgetFillings(){
        BagelSandwich s = new BagelSandwich ("lettuce, bacon");
        assertEquals("Wrong filling returned","lettuce, bacon", s.getFilling() );
    }

    @Test
    public void bagelSandwichaddFillings_getFilling(){
        BagelSandwich s = new BagelSandwich ("lettuce, bacon");
        s.addFilling("tomato");
        assertEquals("Wrong filling returned","lettuce, bacon, tomato", s.getFilling() );
    }

    @Test
    public void bagelSandwichNoFillingsaddFillings_getFilling(){
        BagelSandwich s = new BagelSandwich ("");
        s.addFilling("tomato");
        assertEquals("Wrong filling returned","tomato", s.getFilling() );
    }

    @Test
    public void bagelSandwichFillingsaddFillings_getFilling(){
        BagelSandwich s = new BagelSandwich ("");
        s.addFilling("tomato");
        s.addFilling("lettuce");
        assertEquals("Wrong filling returned","tomato, lettuce", s.getFilling() );
    }

    @Test
    public void isVegetarianSandwich(){
        BagelSandwich s = new BagelSandwich ("lettuce, bacon");
        assertFalse(s.isVegetarian());
    }

}
